// const items = [1, 2, 3, 4, 5, 5];

function filter(item , callback) {
    if (!Array.isArray(item) || item.length == 0) {
        return [];
    }
    let filterArray = [];
    for (let index=0; index<item.length; index++) {
        let callBackValue=callback(item[index],index,item);
        if (callBackValue===true) {
            filterArray.push(item[index]);
        }
    }
    return filterArray;
}

module.exports = filter;