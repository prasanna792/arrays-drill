let filter = require('../filter.cjs');

const items = [1, 2, 3, 4, 5, 5];

function callback(value,index,items) {
    if (value < 4) {
        return true;
    }
}

let result = filter(items, callback);
console.log(result);
