let reduce = require('../reduce.cjs');

const items = [1, 2, 3, 4, 5, 5];

function cb(startValue, value) {
    return startValue+value;
}

let result = reduce(items, cb);

console.log(result);
