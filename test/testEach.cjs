let each = require('../each.cjs');

const items = [1, 2, 3, 4, 5, 5];

function callback(value) {
    console.log(value);
}

let result = each(items, callback);
console.log(result);