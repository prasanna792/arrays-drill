// const items = [1, 2, 3, 4, 5, 5];

function find(item, callback) {
    if(item.length==0){
        return undefined;
    }
    if(!Array.isArray(item)){
        return [];
    }
    for (let index of item) {
        if (callback(index, item)) {
            return index;
        }
        else {
            return undefined;
        }
    }
}



module.exports = find;