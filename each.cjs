// const items = [1, 2, 3, 4, 5, 5];


function each(item , callback) {
    if (!Array.isArray(item) || item.length == 0) {
        return 0;
    }

    for (let index in item) {
        callback(item[index]);
    }
}

module.exports = each;
